(function () {
  var fillers = {};

  fillers.arrays = function () {
    if (!Array.hasOwnProperty("isArray")) {
      Array.isArray = function (obj) {
        return Object.prototype.toString.call(obj) === "[object Array]";
      };
    }

    if (!Array.prototype.hasOwnProperty("forEach")) {
      Array.prototype.forEach = function (fn, context) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this.hasOwnProperty(i)) {
            fn.call(context, this[i], i, this);
          }
        }
      };
    }

    if (!Array.prototype.hasOwnProperty("some")) {
      Array.prototype.some = function (fn, context) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this.hasOwnProperty(i)) {
            if (fn.call(context, this[i], i, this)) {
              return true;
            }
          }
        }

        return false;
      };
    }

    if (!Array.prototype.hasOwnProperty("every")) {
      Array.prototype.every = function (fn, context) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this.hasOwnProperty(i)) {
            if (!fn.call(context, this[i], i, this)) {
              return false;
            }
          }
        }

        return true;
      };
    }

    if (!Array.prototype.hasOwnProperty("filter")) {
      Array.prototype.filter = function (fn, context) {
        var results = [],
          i, length, value;

        for (i = 0, length = this.length; i < length; i++) {
          value = this[i];

          if (fn.call(context, value, i, this)) {
            results.push(value);
          }
        }

        return results;
      };
    }

    if (!Array.prototype.hasOwnProperty("map")) {
      Array.prototype.map = function (fn, context) {
        var results = [],
          i, length, value;

        for (i = 0, length = this.length; i < length; i++) {
          results[i] = fn.call(context, this[i], i, this);
        }

        return results;
      };
    }

    if (!Array.prototype.hasOwnProperty("indexOf")) {
      Array.prototype.indexOf = function (searchElement, fromIndex) {
        var length = this.length,
          startingIndex = fromIndex || 0;

        if (startingIndex < 0) {
          startingIndex = length;
        }

        for (; startingIndex < length; startingIndex++) {
          if (this.hasOwnProperty(startingIndex)) {
            if (this[startingIndex] === searchElement) {
              return startingIndex;
            }
          }
        }

        return -1;
      };
    }

    if (!Array.prototype.hasOwnProperty("lastIndexOf")) {
      Array.prototype.lastIndexOf = function (searchElement, fromIndex) {
        var length = this.length,
          startingIndex = fromIndex || length;

        if (startingIndex < 0) {
          startingIndex = length + startingIndex;
        }

        while (startingIndex--) {
          if (this.hasOwnProperty(startingIndex)) {
            if (this[startingIndex] === searchElement) {
              return startingIndex;
            }
          }
        }

        return -1;
      };
    }

    if (!Array.prototype.hasOwnProperty("reduce")) {
      Array.prototype.reduce = function (fn, initialValue) {
        var isValueSet = false,
          value, i, length;

        if (initialValue) {
          value = initialValue;
          isValueSet = true;
        }

        for (i = 0, length = this.length; i < length; i++) {
          if (this.hasOwnProperty(i)) {
            if (isValueSet) {
              value = fn(value, this[i], i, this);
            } else {
              value = this[i];
              isValueSet = true;
            }
          }
        }

        if (!isValueSet) {
          throw new TypeError("Reduce of empty array with no inital value.");
        }

        return value;
      };
    }

    if (!Array.prototype.hasOwnProperty("reduceRight")) {
      Array.prototype.reduceRight = function (fn, initialValue) {
        var isValueSet = false,
          length = this.length,
          i = length,
          value;

        if (initialValue) {
          value = initialValue;
          isValueSet = true;
        }

        while (i--) {
          if (this.hasOwnProperty(i)) {
            if (isValueSet) {
              value = fn(value, this[i], i, this);
            } else {
              value = this[i];
              isValueSet = true;
            }
          }
        }

        if (!isValueSet) {
          throw new TypeError("Reduce of empty array with no initial value.");
        }

        return value;
      };
    }
  };

  fillers.dates = function () {
    if (!Date.hasOwnProperty("now")) {
      Date.now = function () {
        return (new Date()).getTime();
      };
    }

    if (!Date.prototype.hasOwnProperty("toISOString")) {
      Date.prototype.toISOString = function () {
        function pad(number) {
          return number < 10 ? "0" + number : number;
        }

        return this.getUTCFullYear() + "-" +
          pad(this.getUTCMonth() + 1) + "-" +
          pad(this.getUTCDate()) + "T" +
          pad(this.getUTCHours()) + ":" +
          pad(this.getUTCMinutes()) + ":" +
          pad(this.getUTCSeconds()) + "Z";
      };
    }

    if (!Date.prototype.hasOwnProperty("toJSON")) {
      Date.prototype.toJSON = function () {
        return this.toISOString();
      };
    }
  };

  fillers.objects = function () {
    if (!Object.hasOwnProperty("keys")) {
      Object.keys = function (object) {
        var results = [],
          propertyName;

        for (propertyName in object) {
          if (Object.prototype.hasOwnProperty.call(object, propertyName)) {
            results.push(propertyName);
          }
        }

        return results;
      }
    }

    if (!Object.hasOwnProperty("defineProperty")) {
      // Unfortunately cannot handle descriptors in a non-ES5 environment.
      Object.defineProperty = function (object, propertyName, descriptor) {
        object[propertyName] = descriptor.value || undefined;
      };
    }

    if (!Object.hasOwnProperty("defineProperties")) {
      Object.defineOwnProperties = function (object, descriptors) {
        Object.keys(descriptors).forEach(function (key) {
          Object.defineProperty(object, key, descriptors[key]);
        });
      };
    }

    if (!Object.hasOwnProperty("create")) {
      Object.create = function (object, properties) {
        var result;

        function Fn() {}
        Fn.prototype = object;

        result = new Fn();
        if (properties !== undefined) {
          Object.defineProperties(object, properties);
        }

        return result;
      }
    }
  };

  fillers.functions = function () {
    if (!Function.prototype.hasOwnProperty("bind")) {
      Function.prototype.bind = function (context) {
        var args = Array.prototype.slice.call(arguments, 1),
          bindee = this,
          NoOp = function () {},
          binder = function () {
            return bindee.apply(this instanceof NoOp && context ? this : context, args.concat(Array.prototype.slice.call(arguments)));
          };

        NoOp.prototype = this.prototype;
        binder.prototype = new NoOp();

        return binder;
      };
    }
  };

  fillers.strings = function () {
    if (String.prototype.hasOwnProperty("trim")) {
      String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/gm, "");
      }
    }
  };

  fillers.all = function () {
    this.arrays();
    this.dates();
    this.objects();
    this.functions();
    this.strings();
  };

  if (typeof define === "function" && define.amd) {
    define(fillers);
  } else if (typeof module === "function" && module.exports) {
    module.exports = fillers;
  } else {
    window.fillers = fillers;
  }
}());